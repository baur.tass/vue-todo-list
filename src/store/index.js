import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: [{
      date: "2020-06-25",
      id:0,
      title: "Выполнить тестовое задание",
      status: "new",
      priority: 1,
    }],
  },
  mutations: {
    addTodo: (state, payload)=>{
      payload.id = state.todoList.length;
      state.todoList.push(payload);
    },
    markAsDone: (state, payload) => {
      let findIndex = state.todoList.findIndex( item => item.id == payload)
      state.todoList[findIndex].status = 'done'
    },
    deleteTodo: (state, payload) => {
      let findIndex = state.todoList.findIndex( item => item.id == payload)
      state.todoList.splice(findIndex,1);
    },
    saveTodoChanges: (state, payload) => {
      let findIndex = state.todoList.findIndex( item => item.id == payload)
      state.todoList[findIndex] = payload;
    }
  },
  actions: {
    add: (context, payload) => {
      context.commit('addTodo', payload);
    },
    done: (context, payload) => {
      context.commit('markAsDone', payload);
    },
    delete: (context, payload) => {
      context.commit('deleteTodo', payload);
    },
    save: (context, payload) => {
      context.commit('saveTodoChanges', payload)
    }

  },
  modules: {
  },
  getters: {
    todoList(state){
      return state.todoList;
    }
  }
})
